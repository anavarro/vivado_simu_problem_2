-------------------------------------------------------------------------
-- INTERFACE PACKAGE: Defines types used in the module's ports
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.trigger_pkg.all;
use work.matcher_pkg.all;

package matcher_queue_interface_pkg is
  
  type matcher_queue_in_t is record
    new_wave      : std_logic;
    valid         : std_logic;
    subwave_pairs : std_logic_vector(SUBWAVE_PAIRS_SIZE-1 downto 0);
    subwave_i     : unsigned(SUBWAVES_NUM_BITS-1 downto 0);
    bx_addr_arr   : bx_addr_arr_t;
  end record;  
  
  constant MATCHER_QUEUE_IN_SIZE : natural := 1 + 1 + SUBWAVE_PAIRS_SIZE + SUBWAVES_NUM_BITS + BX_ADDR_ARR_SIZE;
  
  constant MATCHER_QUEUE_IN_NULL : matcher_queue_in_t := (
    new_wave      => '0',
    valid         => '0',
    subwave_pairs => (others => '0'),
    subwave_i     => (others => '0'),
    bx_addr_arr   => (others => (others => '0'))
  );

  type matcher_queue_out_t is record
    enc_matches_queue : match_enc_arr_t (NUM_FITTERS-1 downto 0);
    bx_addr_arr       : bx_addr_arr_t;
  end record;  

  
end package;


-------------------------------------------------------------------------
-- ENTITY
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

library work;
use work.trigger_pkg.all;
use work.matcher_pkg.all;
use work.misc_utils.all;
use work.matcher_queue_interface_pkg.all;


entity matcher_queue is
  port(
    clk       : in  std_logic;
    rst       : in  std_logic;
    input     : in  matcher_queue_in_t;
    output    : out matcher_queue_out_t; 
    q_rden    : in std_logic_vector (NUM_FITTERS-1 downto 0)
	);
end entity;

architecture Behavioural of matcher_queue is
  
  constant FF_WIDTH : natural := MATCHER_QUEUE_IN_SIZE;
  
  signal ff_wren, ff_rden, ff_empty : std_logic;
  signal ff_din, ff_dout_slv : std_logic_vector(FF_WIDTH-1 downto 0);
  signal ff_dout : matcher_queue_in_t;

  -- Type and functions to serialize/deserialize fifo data
  type matcher_queue_in_combo_t is record
    obj : matcher_queue_in_t;
    slv : std_logic_vector(MATCHER_QUEUE_IN_SIZE-1 downto 0);
  end record;

  function flatten_unflatten_input(
    obj : matcher_queue_in_t                                 := MATCHER_QUEUE_IN_NULL     ;
    slv : std_logic_vector(MATCHER_QUEUE_IN_SIZE-1 downto 0) := (others => '0') )
  return matcher_queue_in_combo_t is
    variable combo : matcher_queue_in_combo_t;
    variable cursor : integer range MATCHER_QUEUE_IN_SIZE downto 0 := 0;
  begin
    combo.obj.new_wave := 
          slv( 1 + cursor - 1 ) ;
    combo.slv( 1 + cursor - 1 ) := obj.new_wave ;
    cursor :=  1 + cursor; 

    combo.obj.valid := 
          slv( 1 + cursor - 1 ) ;
    combo.slv( 1 + cursor - 1 ) := obj.valid ;
    cursor :=  1 + cursor; 

    combo.obj.subwave_pairs := 
          slv( SUBWAVE_PAIRS_SIZE + cursor - 1 downto cursor ) ;
    combo.slv( SUBWAVE_PAIRS_SIZE + cursor - 1 downto cursor ) :=
      std_logic_vector( obj.subwave_pairs );
    cursor :=  SUBWAVE_PAIRS_SIZE + cursor;

    combo.obj.subwave_i := unsigned(
          slv(  SUBWAVES_NUM_BITS + cursor - 1 downto cursor ) );
    combo.slv(  SUBWAVES_NUM_BITS + cursor - 1 downto cursor ) :=
      std_logic_vector( obj.subwave_i );
    cursor :=   SUBWAVES_NUM_BITS + cursor;
    
    for i in combo.obj.bx_addr_arr'range loop 
      combo.obj.bx_addr_arr(i) := 
            slv(  N_BIT_BX_ADDR + cursor - 1 downto cursor ) ;
      combo.slv(  N_BIT_BX_ADDR + cursor - 1 downto cursor ) :=
        obj.bx_addr_arr(i);
      cursor :=   N_BIT_BX_ADDR + cursor; 
    end loop;
  
    return combo;
  end function;

  function flatten_input(obj : matcher_queue_in_t) return std_logic_vector is
  begin
    return flatten_unflatten_input( obj => obj ).slv;
  end function;

  function unflatten_input(slv : std_logic_vector) return matcher_queue_in_t is
  begin
    return flatten_unflatten_input( slv => slv ).obj;
  end function;




  ---------------------------------------
  -- Registers
  --
  
  type subsubwave_stage_t is record
    new_wave            : std_logic;
    bx_addr_arr         : bx_addr_arr_t;
    has_new_items       : std_logic;
    num_items_minus_1   : unsigned(SUBSUBWAVE_SIZE_BITS-1 downto 0);
    new_items           : match_enc_arr_t(SUBSUBWAVE_SIZE-1 downto 0);
    subwave_pairs       : std_logic_vector(SUBWAVE_PAIRS_SIZE-1 downto 0);
  end record;
  
  constant SUBSUBWAVE_STAGE_NULL : subsubwave_stage_t := (
    new_wave            => '0',
    bx_addr_arr         => (others => (others => '0')),
    has_new_items       => '0',
    num_items_minus_1   => (others => '0'),
    new_items           => (others => MATCH_ENC_NULL),
    subwave_pairs       => (others => '0')
  );
  
  type subsubwave_pipeline_t is array(SUBSUBWAVE_SIZE downto 0) of subsubwave_stage_t;
  
  type reg_t is record
    i1_empty            : std_logic;
    i1_new_wave         : std_logic;
    i1_subwave_i        : unsigned(SUBWAVES_NUM_BITS-1 downto 0);
    i1_bx_addr_arr      : bx_addr_arr_t;
    i1_subwave_pairs    : std_logic_vector(SUBWAVE_PAIRS_SIZE-1 downto 0);
    i1_subsubwave_heads : std_logic_vector(SUBWAVE_PAIRS_SIZE-1 downto 0);
    i1_num_rem_pairs    : unsigned(SUBWAVE_PAIRS_BITS-1 downto 0);
    i1_n_rem_lt_sswsize : std_logic;
    i1_holds_rem        : unsigned(WAVE_HOLD_CTR_BITS-1 downto 0);
    
    ssw_pipeline        : subsubwave_pipeline_t;
    
    queue               : match_enc_arr_t  (QUEUE_SIZE-1 downto 0);
    bx_addr_arr         : bx_addr_arr_t;
    
    o_queue             : match_enc_arr_t  (QUEUE_SIZE-1 downto 0);
    o_bx_addr_arr       : bx_addr_arr_t;
  end record;
  
  constant REG_NULL : reg_t := (
    i1_empty            => '1',
    i1_new_wave         => '0',
    i1_subwave_i        => (others => '0'),
    i1_bx_addr_arr      => (others => (others => '0')),
    i1_subwave_pairs    => (others => '0'),
    i1_subsubwave_heads => (others => '0'),
    i1_num_rem_pairs    => (others => '0'),
    i1_n_rem_lt_sswsize => '0',
    i1_holds_rem        => (others => '0'),
    
    ssw_pipeline        => (others => SUBSUBWAVE_STAGE_NULL),
    
    queue               => (others => MATCH_ENC_NULL),
    bx_addr_arr         => (others => (others => '0')),
    
    o_queue             => (others => MATCH_ENC_NULL),
    o_bx_addr_arr       => (others => (others => '0'))
  );
  
  signal reg, nextReg : reg_t := REG_NULL;

begin

  -- xpm_fifo_sync: Synchronous FIFO
  -- Xilinx Parameterized Macro, version 2018.3
  input_fifo_inst : xpm_fifo_sync
  generic map (
    -- Sizes
    FIFO_WRITE_DEPTH    => 2**9,
    WRITE_DATA_WIDTH    => FF_WIDTH,
    READ_DATA_WIDTH     => FF_WIDTH,
    PROG_EMPTY_THRESH   => 10,
    PROG_FULL_THRESH    => 10,     -- absolute, not relative to WRITE_DEPTH
    RD_DATA_COUNT_WIDTH => 1,
    WR_DATA_COUNT_WIDTH => 1,
    -- Configuration
    READ_MODE           => "fwft",  -- std / fwft
    FIFO_READ_LATENCY   => 0,     -- 0 if fwft
    FIFO_MEMORY_TYPE    => "auto", -- auto / block / distributed
    USE_ADV_FEATURES    => "0000",
      -- ("000" & data_valid) & (almost_empty & rd_data_count & prog_empty & underflow) & 
      -- ("000" & wr_ack) & (almost_full & wr_data_count & prog_full & overflow)  
    ECC_MODE            => "no_ecc",       -- no_ecc / en_ecc
    DOUT_RESET_VALUE    => "0",
    FULL_RESET_VALUE    => 0,
    WAKEUP_TIME         => 0            -- 0 disable, 2 use sleep pin
  )
  port map (
    rst     => rst,
    wr_clk  => clk,
    -- Input
    wr_en   => ff_wren,
    din     => ff_din,
    full    => open,
    -- Output
    rd_en   => ff_rden,
    dout    => ff_dout_slv,
    empty   => ff_empty,
    -- unused features
    almost_full => open,
    rd_data_count => open,
    wr_data_count => open,
    data_valid => open,
    dbiterr => open,
    overflow => open,
    prog_empty => open,
    prog_full => open,
    rd_rst_busy => open,
    sbiterr => open,
    underflow => open,
    wr_ack => open,
    wr_rst_busy => open,
    injectdbiterr => '0',
    injectsbiterr => '0',
    sleep => '0'
  );
  
  ff_wren <= input.valid;
  ff_din <= flatten_input(input);
  ff_dout <= unflatten_input(ff_dout_slv);
  
  
  process(reg, ff_dout, ff_empty, q_rden, rst)
    variable do_read_i1 : boolean;
    type counts_t is array(SUBWAVE_PAIRS_SIZE-1 downto 0) of unsigned(SUBWAVE_PAIRS_BITS-1 downto 0);
    variable counts : counts_t;
    
    variable last_stage : subsubwave_stage_t;
    variable queue_mux_src : match_enc_arr_t  (QUEUE_SIZE+SUBSUBWAVE_SIZE-1 downto 0);
    
    variable nextReg_o_queue : match_enc_arr_t  (QUEUE_SIZE-1 downto 0);
    variable nextReg_o_bx_addr_arr : bx_addr_arr_t;
  begin
    -- Default values.
    nextReg <= reg;

    ---------------------------
    -- Read from FIFO?
    ---------------------------
    do_read_i1 := (rst = '0') and (
      reg.i1_empty = '1'
      or (reg.i1_holds_rem = 0)
      or reg.i1_n_rem_lt_sswsize = '1' );

    ff_rden <= (not ff_empty) and bo2sl(do_read_i1) and not rst;

    ---------------------------
    -- Clock cycle #1a (from FIFO)
    ---------------------------
    if do_read_i1 then
      nextReg.i1_empty          <= ff_empty;
      nextReg.i1_new_wave       <= ff_dout.new_wave;
      nextReg.i1_subwave_i      <= ff_dout.subwave_i;
      nextReg.i1_bx_addr_arr    <= ff_dout.bx_addr_arr;
      nextReg.i1_subwave_pairs  <= ff_dout.subwave_pairs;
      nextReg.i1_holds_rem      <= to_unsigned(WAVE_MAX_NUM_HOLDS, WAVE_HOLD_CTR_BITS);
      
      -- calculate subsubwave_heads -> marks which positions are the starting point for a subsubwave
      counts := (others => (others => '0'));
      for i in counts'range loop
        for j in 0 to i loop
          counts(i) := counts(i) + sl2int( ff_dout.subwave_pairs(j) );
        end loop;
      end loop;
      for i in reg.i1_subsubwave_heads'range loop
        nextReg.i1_subsubwave_heads(i) <= bo2sl(
          i>0 and any_one( ff_dout.subwave_pairs(i-1 downto 0) ) -- exclude the first head
          and counts(  i)(SUBSUBWAVE_SIZE_BITS-1 downto 0) = to_unsigned(1, SUBSUBWAVE_SIZE_BITS)
          and counts(i-1)(SUBSUBWAVE_SIZE_BITS-1 downto 0) = to_unsigned(0, SUBSUBWAVE_SIZE_BITS));
      end loop;
      
      nextReg.i1_num_rem_pairs <= counts(counts'high);
      nextReg.i1_n_rem_lt_sswsize <= bo2sl( counts(counts'high) <= SUBSUBWAVE_SIZE );
      
    ---------------------------
    -- Clock cycle #1b (from reg.i1_*)
    ---------------------------
    elsif reg.i1_empty = '0' then
      -- i1_new_wave must be active only for 1 clock cycle even if the 1st word of the new wave is held for various cycles
      nextReg.i1_new_wave <= '0';
      
      -- if the state is being held, reduce the counter of available hold cycles by 1
      nextReg.i1_holds_rem <= reg.i1_holds_rem - 1;
      
      for i in reg.i1_subwave_pairs'range loop
        nextReg.i1_subwave_pairs   (i) <= reg.i1_subwave_pairs   (i) and
            bo2sl(        reg.i1_subsubwave_heads(i   downto 0) /= (i   downto 0 => '0'));
        nextReg.i1_subsubwave_heads(i) <= reg.i1_subsubwave_heads(i) and
            bo2sl(i>0 and reg.i1_subsubwave_heads(i-1 downto 0) /= (i-1 downto 0 => '0'));
      end loop;
    
      -- if less than SUBSUBWAVE_SIZE pairs were written to funnel is because they were not
      -- available, thus this sub-wave is exhausted, and a do_read will be done
      nextReg.i1_num_rem_pairs <= reg.i1_num_rem_pairs - SUBSUBWAVE_SIZE;
      nextReg.i1_n_rem_lt_sswsize <= bo2sl( reg.i1_num_rem_pairs <= 2 * SUBSUBWAVE_SIZE );
    end if;

    ---------------------------
    -- Clock cycle 2 (i1->pipeline)
    ---------------------------
    nextReg.ssw_pipeline(0) <= SUBSUBWAVE_STAGE_NULL;
    if reg.i1_empty = '0' then
      nextReg.ssw_pipeline(0).new_wave <= reg.i1_new_wave; 
      nextReg.ssw_pipeline(0).bx_addr_arr <= reg.i1_bx_addr_arr;
      nextReg.ssw_pipeline(0).has_new_items <= bo2sl(reg.i1_num_rem_pairs > 0);
      
      if reg.i1_num_rem_pairs >= SUBSUBWAVE_SIZE then
        nextReg.ssw_pipeline(0).num_items_minus_1 <= to_unsigned(SUBSUBWAVE_SIZE-1, reg.ssw_pipeline(0).num_items_minus_1'length);
      else
        nextReg.ssw_pipeline(0).num_items_minus_1 <= noop(reg.i1_num_rem_pairs - 1)(reg.ssw_pipeline(0).num_items_minus_1'range);
      end if;
      nextReg.ssw_pipeline(0).new_items <= (others => (subwave_i => reg.i1_subwave_i,pair_i => (others => '0'), valid => '1'));
      nextReg.ssw_pipeline(0).subwave_pairs <= reg.i1_subwave_pairs;
    end if;

    ---------------------------
    -- Process the pipeline
    ---------------------------
    for stg_i in 1 to reg.ssw_pipeline'high loop
      nextReg.ssw_pipeline(stg_i).new_wave          <= reg.ssw_pipeline(stg_i-1).new_wave;
      nextReg.ssw_pipeline(stg_i).bx_addr_arr       <= reg.ssw_pipeline(stg_i-1).bx_addr_arr;
      nextReg.ssw_pipeline(stg_i).has_new_items     <= reg.ssw_pipeline(stg_i-1).has_new_items;
      nextReg.ssw_pipeline(stg_i).num_items_minus_1 <= reg.ssw_pipeline(stg_i-1).num_items_minus_1;
      
      nextReg.ssw_pipeline(stg_i).new_items         <= reg.ssw_pipeline(stg_i-1).new_items;
      nextReg.ssw_pipeline(stg_i).subwave_pairs     <= reg.ssw_pipeline(stg_i-1).subwave_pairs;

      for i in reg.ssw_pipeline(stg_i).subwave_pairs'high downto 0 loop
        if reg.ssw_pipeline(stg_i-1).subwave_pairs(i) = '1' then
          nextReg.ssw_pipeline(stg_i).new_items(SUBSUBWAVE_SIZE-stg_i).pair_i <= to_unsigned(i, SUBWAVE_PAIRS_BITS);
        end if;
        if i = 0 or reg.ssw_pipeline(stg_i-1).subwave_pairs(i-1 downto 0) = (i-1 downto 0 => '0') then
          nextReg.ssw_pipeline(stg_i).subwave_pairs(i) <= '0';
        end if; 
      end loop;
    end loop;
    
    ---------------------------
    -- From end of pipeline to the queue
    ---------------------------
    last_stage := reg.ssw_pipeline(reg.ssw_pipeline'high);

    if last_stage.new_wave = '1' then
      nextReg.queue <= (others => MATCH_ENC_NULL);
      queue_mux_src := (others => MATCH_ENC_NULL);
      queue_mux_src(last_stage.new_items'range) := last_stage.new_items;
      nextReg.bx_addr_arr <= last_stage.bx_addr_arr; -- save the bx_addr_arr of the wave for transfer to output stage on next wave
    else
      queue_mux_src := reg.queue & last_stage.new_items;
    end if;
    
    for i in 0 to QUEUE_SIZE-1 loop
      if last_stage.has_new_items = '1' then
        nextReg.queue(i) <= queue_mux_src(SUBSUBWAVE_SIZE+i-(to_integer(last_stage.num_items_minus_1)+1));
      end if;
    end loop;

    ---------------------------
    -- Shift the Queue and output the matches
    ---------------------------
    nextReg_o_queue := reg.o_queue;
    nextReg_o_bx_addr_arr := reg.o_bx_addr_arr;
    if last_stage.new_wave = '1' then
      nextReg_o_queue := reg.queue;
      nextReg_o_bx_addr_arr := reg.bx_addr_arr;
    else
      for i in 0 to q_rden'high loop
        if q_rden(i) = '1' then
          nextReg_o_queue := MATCH_ENC_NULL & nextReg_o_queue(nextReg_o_queue'high downto 1);
        end if;
      end loop;
    end if;
    
    nextReg.o_queue <= nextReg_o_queue;
    nextReg.o_bx_addr_arr <= nextReg_o_bx_addr_arr;
    
    output.enc_matches_queue <= reg.o_queue(output.enc_matches_queue'range);
    output.bx_addr_arr       <= reg.o_bx_addr_arr;

  end process;
  
  process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then 
        reg <= REG_NULL;
      else--elsif enable = '1' then
        reg <= nextReg;
      end if;
    end if;
  end process;


end architecture;
