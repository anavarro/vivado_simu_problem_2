-- This module just replicates your top inputs and outputs, and registers them, in order to
-- impose timing restrictions on them, so that if you synthetise it, you will get a timing
-- report 

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.VComponents.all;

library work;
use work.trigger_pkg.all;
use work.matcher_interface_pkg.all;

entity matcher_wrapper is
  port(
    clk               : in  std_logic;
    rst               : in  std_logic;
    conf              : in  ControlRegs_t;
    input             : in  matcher_in_t;
    output            : out matcher_out_t
	);
end entity;

architecture Behavioural of matcher_wrapper is

  signal input_s  : matcher_in_t;
  signal output_s : matcher_out_t;

begin

  matcher_inst: entity work.matcher
  port map(
    clk               => clk                ,
    rst               => rst                ,
    conf              => conf               ,
    input             => input_s            ,
    queue_out_arr     => output_s
  );

  process(clk)
  begin
    if rising_edge(clk) then
      input_s <= input    ;
      output  <= output_s ;
    end if;
  end process;

end architecture;
