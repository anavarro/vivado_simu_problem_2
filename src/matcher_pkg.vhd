library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.trigger_pkg.all;
use work.misc_utils.all;

-------------------------------------------------------------------------------
-- Matcher overview
-- 
-- The matching consists
-- 1. Matching Matrix
--    The segments from SL1 and SL3 come in waves, each wave corresponding to a BX
--    Each wave has a certain number of segments. Once this segments are received,
--    segments from SL1 and SL3 are paired together, and also with segments from
--    the opposite SL from past wave(s). This pairing is not done in one clock cycle
--    Instead, a wave-like processing each done: one segment from each SL is taken 
--    each time as base for matchings, pairing it with everything in the opposite SL
--    For each pairing, an a-priori quality is evaluated from their relative T0s,
--    positions and slopes, and also qualities 
--    Then for each of the possible pairing qualities, the array of whether each
--    sub-wave pairings is in this quality or not is passed to a matcher_queue module
-- 2. Matcher queue
--    Essentialy a serializer
--    The matcher queue receives, for each wave, several sub-waves of arrays of bits
--    that mark whether the corresponding pairing belongs to this queue because of its quality
--    It must calculate the index of each of these valid bits, and output a fifo-like
--    interface in which it makes avalilable a succession of indexes that identify the 
--    pairings in this queue. In fact, the pairings are translated to addresses of the RAMs
--    that store the complete segments information for latter fitting
-- 3. Fitting selection 
--    A simple state machine reads from all available per-quality queues, and selects the 
--    best quality pairings for matching, reading from the RAMs and putting the data in the
--    fitters
-- 4. Fitting
--    The instances of the 2xSL fitters themselves
-------------------------------------------------------------------------------

package matcher_pkg is

  -------------------------------------------------------------------------------
  -- Constants that determine the data flow of the matcher
  -- 

  -- The number of segments in each wave. It is actually defined in trigger_pkg because it is
  -- used by the SL filter. Here we do a simple alias to have a shorter name
  constant WAVE_SIZE : natural := NUM_SLSEGS_PER_BX;
  -- The size of an unsigned that acts as a counter for subwaves or segments within a wave
  -- (thus 0 to WAVE_SIZE-1)
  constant WAVE_SIZE_BITS  : natural := unsigned_size_to_fit(WAVE_SIZE-1); -- up to 6 subwaves
  
  -- When each segment arrives, it is stored to a RAM for later use. 
  -- The number of LSB of the BX used for addressing in the memories
  constant N_BIT_BX_ADDR : integer := 4;
  -- final RAM address field width
  constant RAM_ADDR_WIDTH : integer := N_BIT_BX_ADDR + WAVE_SIZE_BITS;

  -- The number of pairings done in each of the WAVE_SIZE iterations
  -- Each iteration, a single segment is chosen from each SL latest wave:
  --   - The one from SL1 is matched with all segments from SL3, present and past
  --   - The one from SL3 is matched with all past sgements from SL1             
  
  -- The number of waves (both present and past) considered in the Matching Matrix.
  constant PAST_WAVES_NUM            : natural := 2 ;
  constant CURR_AND_PAST_WAVES_TOTAL : natural := 1 + PAST_WAVES_NUM ;
  
  -- The number of crossings that must be done between (Wave_i,SL_j) blocks of segments
  -- The minus one is because the parings between the two SLs in the current wave have to
  -- be done only once
  constant W2W_XINGS          : natural := 2 * CURR_AND_PAST_WAVES_TOTAL - 1; -- 5
  
  -- Each subwave in the Matching Matrix and the Queue has this many pairings:
  -- For each of the crossing between blocks, there is one pairing for each of
  -- the secondary block segments (only one segment is considered in the primary
  -- block, this is why sub-waves exist at all)
  constant SUBWAVE_PAIRS_SIZE : natural := WAVE_SIZE * W2W_XINGS; -- 30
  -- The size of an unsigned that can hold the previous number
  constant SUBWAVE_PAIRS_BITS : natural := unsigned_size_to_fit(SUBWAVE_PAIRS_SIZE); -- 0-30 --> 5 bits
  -- Another alias for WAVE_SIZE, makes code more readable. The number of subwaves
  -- is exactly the size of the waves, because one sub-wave must be done for each
  -- base segment of the primary block, and there are WAVE_SIZE of these
  constant SUBWAVES_NUM       : natural := WAVE_SIZE; -- 6
  -- The size of an unsigned that acts as a counter for subwaves (thus 0 to SUBWAVES_NUM-1)
  constant SUBWAVES_NUM_BITS  : natural := unsigned_size_to_fit(SUBWAVES_NUM-1); -- up to 6 subwaves

  -- Inside the matcher_queue, a queue is built. The maximum number of useful positions is 
  -- NUM_FITTERS * CLK_RATIO, which is the number of fits that we can do per BX, but shorter
  -- queues make more sense because it is very unlikely that all of the useful fits come from the
  -- same queue
  constant QUEUE_SIZE       : natural := 16; -- could be up to 24, but 16 is more than enough
  
  -- Each sub-wave is processed on several sub-sub waves, each one taking care of adding to the queue 
  -- SUBSUBWAVE_SIZE pairings. Because of how it's implemented, it is convenient that this number is a power of 2.
  -- When there is more than one sub-sub-wave in a sub-wave, the data is held until all the sub-sub-waves
  -- have been launched. However, there is a limit to this, because a wave must be processed in CLK_RATIO cycles,
  -- and since there are SUBWAVES_NUM sub-waves in a wave, each one already consumes 1 cycle, regardless of the
  -- usefulness of the data in it.
  constant WAVE_MAX_NUM_HOLDS : natural := CLK_RATIO - WAVE_SIZE; --6
  -- The size of an unsigned that acts as a counter for the amount of cycles that can be held
  constant WAVE_HOLD_CTR_BITS : natural := unsigned_size_to_fit(WAVE_MAX_NUM_HOLDS); --0-6
  
  -- The total number of items that can added to the queue in matcher_queue depends on the size of the
  -- sub-sub-wave, but also on whether each sub-wave carries valid pairings or not.
  --   - worst case: all but one sub-waves are empty: the max number of items added is
  --     (CLK_RATIO - WAVE_SIZE + 1)*SUBSUBWAVE_SIZE
  --   - best case: all sub-waves contain a multiple of SUBSUBWAVE_SIZE valid pairings
  --     CLK_RATIO*SUBSUBWAVE_SIZE
  -- It's not needed to account for the worst case when choosing SUBSUBWAVE_SIZE, chose a value
  -- so that QUEUE_SIZE is between the worst and best cases.
  constant SUBSUBWAVE_SIZE  : natural := 2;   -- must be a power of 2
  -- The size of an unsigned that can contain SUBSUBWAVE_SIZE-1
  constant SUBSUBWAVE_SIZE_BITS : natural := unsigned_size_to_fit(SUBSUBWAVE_SIZE-1);

  -- The number of fitters at the end of the matcher.
  -- Each fitter module processes 1 fit per clock cycle (at CLK_RATIO)
  -- So the total number of fits that can be processed per BX is
  -- NUM_FITTERS * CLK_RATIO
  constant NUM_FITTERS      : natural := 2;





  -------------------------------------------------------------------------------
  -- Some useful type definitions
  -- 

  -- The array that contains the part of the RAM address that corresponds to the BX lsb
  -- for each of the waves under consideration
  type bx_addr_arr_t is array(CURR_AND_PAST_WAVES_TOTAL-1 downto 0) of std_logic_vector(N_BIT_BX_ADDR-1 downto 0);
  -- The total size of this vector, because it is serialized to a FIFO
  constant BX_ADDR_ARR_SIZE : natural := CURR_AND_PAST_WAVES_TOTAL*N_BIT_BX_ADDR;

  -- Each pairing must contain the info to allow retrieving its data from the RAMs
  -- This information is
  --   - an array (for each SL) of std_logic_vectors (of the width of the RAM address)
  --   - a valid bit
  type match_addr_arr_t is array(1 downto 0) of std_logic_vector(RAM_ADDR_WIDTH-1 downto 0);
  type match_t is record
    addr_arr  : match_addr_arr_t;
    valid     : std_logic;
  end record;
  constant MATCH_NULL : match_t := (
    addr_arr  => (others => (others => '0')),
    valid     => '0'
  );
  -- An array of the aforementioned pairing informations for the queues outputs
  type match_arr_t is array(integer range <>) of match_t;

  -- Inside the queue, the pairings are not stored with the full address information,
  -- but instead in a reduced "encoded" format, which can be later decoded to the
  -- standard format. This encoded format contains:
  --   - subwave_i: the index within the wave from which the pairing comes
  --   - pair_i: the index of the pairing withing the sub-wave
  --   - valid 
  type match_enc_t is record
    subwave_i : unsigned(SUBWAVES_NUM_BITS-1 downto 0);
    pair_i    : unsigned(SUBWAVE_PAIRS_BITS-1 downto 0);
    valid     : std_logic;
  end record;
  constant MATCH_ENC_NULL : match_enc_t := (
    subwave_i => (others => '0'),
    pair_i    => (others => '0'),
    valid     => '0'
  );
  -- An array of the aforementioned encoded pairing informations for the sub-sub-wave and the queue
  type match_enc_arr_t is array(integer range <>) of match_enc_t;
  
  -------------------------------------------------------------------------------
  -- Functions
  -- 
  
  function decode_match(enc : match_enc_t ; bx_addr_arr : bx_addr_arr_t ) return match_t;

end package;

-------------------------------------------------------------------------------
---- Body
-------------------------------------------------------------------------------
package body matcher_pkg is

  function decode_match(enc : match_enc_t ; bx_addr_arr : bx_addr_arr_t ) return match_t is
    variable res: match_t;
    variable sec_wave_i : natural range 0 to CURR_AND_PAST_WAVES_TOTAL-1;
    variable sec_sl_i   : natural range 0 to 1;
    variable sec_seg_i  : natural range 0 to WAVE_SIZE-1;
  begin
    
    sec_wave_i  := ( to_integer(enc.pair_i)  + WAVE_SIZE) / (WAVE_SIZE*2);
    sec_sl_i    := ( to_integer(enc.pair_i)  / WAVE_SIZE) mod 2;
    sec_seg_i   :=   to_integer(enc.pair_i)  mod WAVE_SIZE;
    
    res.addr_arr( 1 - sec_sl_i ) :=
      bx_addr_arr(0) & 
      std_logic_vector(enc.subwave_i);
    res.addr_arr(     sec_sl_i ) :=
      bx_addr_arr(sec_wave_i) & 
      std_logic_vector(to_unsigned((to_integer(enc.subwave_i) + sec_seg_i) mod SUBWAVES_NUM , SUBWAVES_NUM_BITS));
    res.valid := enc.valid;
    
    return res;
  end function;


end package body;