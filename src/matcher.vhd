-------------------------------------------------------------------------
-- INTERFACE PACKAGE: Defines types used in the module's ports
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.trigger_pkg.all;
use work.matcher_pkg.all;
use work.matcher_queue_interface_pkg.all;

package matcher_interface_pkg is

  type superlayer_datapath_out_t is record
    segment_valid     : std_logic;
    segment           : SLsegment_t;
    new_wave          : std_logic;
    wave_bctr         : unsigned(11 downto 0);
  end record;

  constant SUPERLAYER_DATAPATH_OUT_NULL : superlayer_datapath_out_t := (
    segment_valid     => '0',
    segment           => SLSEGMENT_NULL,
    new_wave          => '0',
    wave_bctr         => (others => '0')
  );
  
  type sl_arr_t is array(1 downto 0) of superlayer_datapath_out_t;

  type matcher_in_t is record
    -- Each SL is record
    --  segment_valid (std_logic)
    --  segment (SLsegment_t)
    --  new_wave (std_logic)
    --  wave_bctr (unsigned12)
    sls : sl_arr_t;
  end record;

  constant MATCHER_IN_NULL : matcher_in_t := (
    sls => (others => SUPERLAYER_DATAPATH_OUT_NULL)
  );

  -- The number of different queues in which matcher candidates are classified
  constant N_PRIORITIES : natural := 3;

  type matcher_out_t  is array(N_PRIORITIES-1 downto 0) of matcher_queue_out_t;
  
end package;


-------------------------------------------------------------------------
-- ENTITY
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.trigger_pkg.all;
use work.matcher_pkg.all;
use work.misc_utils.all;
use work.matcher_interface_pkg.all;
use work.matcher_queue_interface_pkg.all;

-- For a detailed explanation on how it works, see matcher_pkg.vhd
entity matcher is
  port(
    clk               : in  std_logic;
    rst               : in  std_logic;
    conf              : in  ControlRegs_t;
    input             : in  matcher_in_t;
    queue_out_arr     : out matcher_out_t
	);
end entity;

architecture Behavioural of matcher is
  
  signal rx_inpt_index  :unsigned(SUBWAVES_NUM_BITS-1 downto 0);
  
  -- Ports of the array of Priority Queues
  type queue_in_arr_t   is array(N_PRIORITIES-1 downto 0) of matcher_queue_in_t;
  type queue_rden_arr_t is array(N_PRIORITIES-1 downto 0) of std_logic_vector(NUM_FITTERS-1 downto 0);
  
  signal queue_in_arr   : queue_in_arr_t;
  signal queue_out_arr_s: matcher_out_t;
  signal queue_rden_arr : queue_rden_arr_t;
  
begin

  ---------------------------------------------------------------
  -- Input stage and matching matrix, generates inputs for array of queues
  --
  input_to_queues_gen: if True generate
  
    ---------------------------------------
    -- Types and constants
    --
  
    -- SL segment reduced information used for assessing potential match quality
    type SLsegProfile_t is record
      valid     : std_logic;
      is4hit    : std_logic;
      t0        : unsigned(4 downto 0); -- (fine tdc)
      position  : signed  (WIDTH_FULL_POS   - 9 - 1 downto 0);
    end record;
    
    constant SLSEGPROFILE_NULL : SLsegProfile_t := (
      valid     => '0',
      is4hit    => '0',
      t0        => (others => '0'),
      position  => (others => '0')
    );
    
    type waveSLprofiles_t is array(WAVE_SIZE - 1 downto 0) of SLsegProfile_t;
    constant WAVESLPROFILES_NULL : waveSLprofiles_t := (others => SLSEGPROFILE_NULL);
    
    type waveProfiles_t is array(1 downto 0) of waveSLprofiles_t;
    constant WAVEPROFILES_NULL : waveProfiles_t := (others => WAVESLPROFILES_NULL);
    
    type mm_WaveProfiles_t is array(CURR_AND_PAST_WAVES_TOTAL-1 downto 0) of waveProfiles_t;
    constant MM_WAVEPROFILES_NULL : mm_WaveProfiles_t := (others => WAVEPROFILES_NULL);
    
    type indexes_arr_t is array(integer range <>) of unsigned(SUBWAVES_NUM_BITS-1 downto 0);
    ---------------------------------------
    -- Registers
    --
    type reg_t is record
      -- input stage
      rx_inpt_index       : unsigned(SUBWAVES_NUM_BITS-1 downto 0);
      rx_waveProfiles     : waveProfiles_t;
      rx_waveBXaddr       : std_logic_vector(N_BIT_BX_ADDR-1 downto 0);
      rx_done             : std_logic;
      
      -- work data for the matching matrix
      mm_new_wave         : std_logic;
      mm_waveProfiles     : mm_WaveProfiles_t; --indexes are (wave age)(superlayer)(index in wave)
      mm_waveBXaddr       : bx_addr_arr_t;
      mm_indexes          : indexes_arr_t(WAVESLPROFILES_NULL'range);
      
      -- output
      o_queue_in_arr      : queue_in_arr_t;
  
    end record;
    
    constant REG_NULL : reg_t := (
      rx_inpt_index       => (others => '0'),
      rx_waveProfiles     => WAVEPROFILES_NULL,
      rx_waveBXaddr       => (others => '0'),
      rx_done             => '0',
      
      mm_new_wave         => '0',
      mm_waveProfiles     => MM_WAVEPROFILES_NULL,
      mm_waveBXaddr       => (others => (others => '0')),
      mm_indexes          => (others => (others => '0')),
      
      o_queue_in_arr      => (others => MATCHER_QUEUE_IN_NULL)
    );
    
    signal reg, nextReg : reg_t := REG_NULL;
    
    ---------------------------------------
    -- Functions
    --
    
    function get_profile (seg : SLsegment_t; valid : std_logic ) return SLsegProfile_t is
      variable seg_profile : SLsegProfile_t := SLSEGPROFILE_NULL;
    begin
      seg_profile.valid     := valid;
      seg_profile.is4hit    := seg.is4hit; 
      seg_profile.t0        := seg.t0      (4 downto 2) & "00"; 
      seg_profile.position  := seg.position(seg.position'high downto seg.position'length - seg_profile.position'length); 
      return seg_profile;
    end function;
  
    function get_priority_onehot (
      seg0 : SLsegProfile_t;
      seg1 : SLsegProfile_t;
      seg1_age : natural range 2 downto 0; -- whether it comes from one of past waves
      conf : ControlRegs_t )
    return std_logic_vector is
      variable prio1hot : std_logic_vector(N_PRIORITIES-1 downto 0) := (others => '0');
    begin
      if seg0.valid = '0' or seg1.valid = '0' then
        return prio1hot;
      elsif (to_unsigned(seg1_age,2)&seg0.t0) - (to_unsigned(0,2)&seg1.t0) > 40 then
        return prio1hot;
      elsif abs(seg1.position - seg0.position) > 1 then
        return prio1hot;
      end if;
      prio1hot(bo2int(seg0.is4hit = '1') + bo2int(seg1.is4hit = '1')) := '1';
      return prio1hot;
    end function;
    
  begin
  
    -- Main state machine
    process(reg, input, conf)
      variable sec_wave_i : natural range 0 to CURR_AND_PAST_WAVES_TOTAL-1;
      variable sec_sl_i   : natural range 0 to 1;
      variable sec_seg_i  : natural range 0 to WAVE_SIZE-1;
    begin
      -- Default values.
      nextReg <= reg;
      
      ---------------------------
      -- Input stage
      ---------------------------
      -- input index goes from 0 to WAVE_SIZE-1 and resets to 0, waiting for next wave
      nextReg.rx_done <= '0';
      if reg.rx_inpt_index = to_unsigned(WAVE_SIZE-1, reg.rx_inpt_index'length) then
        nextReg.rx_inpt_index <= (others => '0');
        nextReg.rx_done <= '1';
      elsif reg.rx_inpt_index > 0 or input.sls(0).new_wave = '1' then
        nextReg.rx_inpt_index <= reg.rx_inpt_index + 1;
      end if;
      
      -- store segment profile to its index
      if reg.rx_inpt_index > 0 or input.sls(0).new_wave = '1' then
        for sl_i in reg.rx_waveProfiles'range loop
            nextReg.rx_waveProfiles(sl_i) <=
              get_profile( input.sls(sl_i).segment, input.sls(sl_i).segment_valid ) &
              reg.rx_waveProfiles(sl_i)(WAVE_SIZE-1 downto 1);
        end loop;
        nextReg.rx_waveBXaddr <= std_logic_vector( input.sls(0).wave_bctr(N_BIT_BX_ADDR-1 downto 0) );
      end if;
      
      -- shift the pipeline of bx waveprofiles and kickstart new matching wave
  
      nextReg.mm_new_wave <= reg.rx_done;
      
      if reg.rx_done = '1' then
        for i in reg.mm_indexes'range loop
          nextReg.mm_indexes(i) <= to_unsigned(i,reg.mm_indexes(i)'length);
        end loop;
        nextReg.mm_waveProfiles <= reg.mm_waveProfiles(MM_WAVEPROFILES_NULL'high-1 downto 0) & reg.rx_waveProfiles;
        nextReg.mm_waveBXaddr   <= reg.mm_waveBXaddr  (MM_WAVEPROFILES_NULL'high-1 downto 0) & reg.rx_waveBXaddr  ;
      elsif reg.mm_new_wave = '1' or reg.mm_indexes(0) > 0 then
        -- Each subwave cycle, all waves are rotated to place the "working segment" in the bottom-most position
        nextReg.mm_indexes <= reg.mm_indexes(0) & reg.mm_indexes(WAVE_SIZE-1 downto 1);
        for wave_i in reg.mm_waveProfiles'range loop
          for sl_i in reg.mm_waveProfiles(wave_i)'range loop
            nextReg.mm_waveProfiles(wave_i)(sl_i) <=
              reg.mm_waveProfiles(wave_i)(sl_i)(0) &
              reg.mm_waveProfiles(wave_i)(sl_i)(WAVE_SIZE-1 downto 1);
          end loop;
        end loop;
      end if;
      
      ---------------------------
      -- Matching Matrix processing
      ---------------------------
      for q_i in N_PRIORITIES-1 downto 0 loop
  
        nextReg.o_queue_in_arr(q_i).new_wave    <= reg.mm_new_wave;
        nextReg.o_queue_in_arr(q_i).valid       <= bo2sl(reg.mm_new_wave = '1' or reg.mm_indexes(0) > 0);
        nextReg.o_queue_in_arr(q_i).bx_addr_arr <= reg.mm_waveBXaddr;
        nextReg.o_queue_in_arr(q_i).subwave_i   <= reg.mm_indexes(0);
  
        for pair_subwave_i in SUBWAVE_PAIRS_SIZE-1 downto 0 loop
          sec_wave_i  := (pair_subwave_i + WAVE_SIZE) / (WAVE_SIZE*2);
          sec_sl_i    := (pair_subwave_i / WAVE_SIZE) mod 2;
          sec_seg_i   := pair_subwave_i mod WAVE_SIZE;

          nextReg.o_queue_in_arr(q_i).subwave_pairs(pair_subwave_i) <=
            get_priority_onehot (
                reg.mm_waveProfiles(         0)(1-sec_sl_i)(        0),
                reg.mm_waveProfiles(sec_wave_i)(  sec_sl_i)(sec_seg_i),
                sec_wave_i, conf
              )(q_i);
        end loop;
      end loop;
  
      queue_in_arr <= reg.o_queue_in_arr;
      rx_inpt_index <= reg.rx_inpt_index;
  
    end process;
    
    process(clk)
    begin
      if rising_edge(clk) then
        if rst = '1' then 
          reg <= REG_NULL;
        else--elsif enable = '1' then
          reg <= nextReg;
        end if;
      end if;
    end process;
  
  end generate;

  ---------------------------------------
  -- Array of per-priority queues
  --

  per_priority_gen : for q_i in 0 to N_PRIORITIES-1 generate
    
    matcher_queue_inst : entity work.matcher_queue
    port map(
      clk       => clk,
      rst       => rst,
      input     => queue_in_arr   (q_i),
      output    => queue_out_arr_s(q_i),
      q_rden    => queue_rden_arr (q_i)
    );
  
  end generate;



  ---------------------------------------
  -- Per-priority queues to output
  --

  process(queue_out_arr_s)
    variable written : natural range NUM_FITTERS downto 0;
  begin
    queue_out_arr <= queue_out_arr_s;

    written := 0;
    queue_rden_arr <= (others => (others => '0'));
    for q_i in queue_out_arr_s'high downto 0 loop
      for out_i in 0 to queue_out_arr_s(0).enc_matches_queue'high loop
        if written < NUM_FITTERS and queue_out_arr_s(q_i).enc_matches_queue(out_i).valid = '1' then
          queue_rden_arr(q_i)(out_i) <= '1';
          written := written + 1;
        end if;
      end loop;
    end loop;
    
  end process;
  

end architecture;
