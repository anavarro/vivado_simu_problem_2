library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.misc_utils.all;

package trigger_pkg is

  ---------------------------------------------------------------
  -- Data format and data flow constants
  ---------------------------------------------------------------
  
  constant WIDTH_FULL_TIME  : natural := 17; 
  constant WIDTH_FULL_POS   : natural := 17;
  constant WIDTH_FULL_SLOPE : natural := 14;
  constant WIDTH_FULL_CHI2 : natural := 16;

  ---------------------------------------------------------------
  -- Constants that control data flow across modules
  --
  -- The ratio of the trigger clock to the LHC clock
  constant CLK_RATIO : natural := 12;
  
  -- How many segments are output from the superlayer branch to the matcher in each BX
  -- Used in the filter and in the matcher. Must be less than or equal to CLK_RATIO.
  -- Really any number bigger than 6 should require to analyze the code to see that nothing
  -- breaks.
  constant NUM_SLSEGS_PER_BX : natural := 6;
  
  ---------------------------------------------------------------
  -- Data types and corresponding NULL constants
  ---------------------------------------------------------------
  
  -- Control registers
  type ControlRegs_t is record
    dummy : std_logic;
  end record;

  constant CONTROLREGS_DEFAULT : ControlRegs_t := (
    dummy => '0'
  );

  ---------------------------------------------------------------
  -- SL segment

  type SLsegment_t is record
    t0              : unsigned(WIDTH_FULL_TIME  - 1 downto 0);
    position        : signed  (WIDTH_FULL_POS   - 1 downto 0);
    is4hit          : std_logic;
  end record;
  
  constant SLSEGMENT_NULL : SLsegment_t := (
    t0              => (others => '0'),
    position        => (others => '0'),
    is4hit          => '0'
  );

  type seg_arr_t is array (integer range <>) of SLsegment_t;
  
end package;

