----------------------------------------------------------------------------------
-- Miscellaneous types, constants, functions 
-- �lvaro Navarro, CIEMAT
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

package misc_utils is

  -- Generic signed, unsigned, std_logic_vector arrays
  type int_arr_t    is array(integer range <>) of integer;

  type usgn2_arr_t  is array(integer range <>) of unsigned( 1 downto 0);
  type usgn3_arr_t  is array(integer range <>) of unsigned( 2 downto 0);
  type usgn4_arr_t  is array(integer range <>) of unsigned( 3 downto 0);
  type usgn5_arr_t  is array(integer range <>) of unsigned( 4 downto 0);
  type usgn6_arr_t  is array(integer range <>) of unsigned( 5 downto 0);
  type usgn7_arr_t  is array(integer range <>) of unsigned( 6 downto 0);
  type usgn8_arr_t  is array(integer range <>) of unsigned( 7 downto 0);
  type usgn9_arr_t  is array(integer range <>) of unsigned( 8 downto 0);
  type usgn10_arr_t is array(integer range <>) of unsigned( 9 downto 0);
  type usgn11_arr_t is array(integer range <>) of unsigned(10 downto 0);
  type usgn12_arr_t is array(integer range <>) of unsigned(11 downto 0);
  type usgn13_arr_t is array(integer range <>) of unsigned(12 downto 0);
  type usgn14_arr_t is array(integer range <>) of unsigned(13 downto 0);
  type usgn15_arr_t is array(integer range <>) of unsigned(14 downto 0);
  type usgn16_arr_t is array(integer range <>) of unsigned(15 downto 0);
  type usgn17_arr_t is array(integer range <>) of unsigned(16 downto 0);
  type usgn18_arr_t is array(integer range <>) of unsigned(17 downto 0);
  type usgn19_arr_t is array(integer range <>) of unsigned(18 downto 0);
  type usgn20_arr_t is array(integer range <>) of unsigned(19 downto 0);
  type usgn21_arr_t is array(integer range <>) of unsigned(20 downto 0);
  type usgn22_arr_t is array(integer range <>) of unsigned(21 downto 0);
  type usgn23_arr_t is array(integer range <>) of unsigned(22 downto 0);
  type usgn24_arr_t is array(integer range <>) of unsigned(23 downto 0);
  type usgn25_arr_t is array(integer range <>) of unsigned(24 downto 0);
  type usgn26_arr_t is array(integer range <>) of unsigned(25 downto 0);
  type usgn27_arr_t is array(integer range <>) of unsigned(26 downto 0);
  type usgn28_arr_t is array(integer range <>) of unsigned(27 downto 0);
  type usgn29_arr_t is array(integer range <>) of unsigned(28 downto 0);
  type usgn30_arr_t is array(integer range <>) of unsigned(29 downto 0);
  type usgn31_arr_t is array(integer range <>) of unsigned(30 downto 0);
  type usgn32_arr_t is array(integer range <>) of unsigned(31 downto 0);

  type sgn2_arr_t   is array(integer range <>) of   signed( 1 downto 0);
  type sgn3_arr_t   is array(integer range <>) of   signed( 2 downto 0);
  type sgn4_arr_t   is array(integer range <>) of   signed( 3 downto 0);
  type sgn5_arr_t   is array(integer range <>) of   signed( 4 downto 0);
  type sgn6_arr_t   is array(integer range <>) of   signed( 5 downto 0);
  type sgn7_arr_t   is array(integer range <>) of   signed( 6 downto 0);
  type sgn8_arr_t   is array(integer range <>) of   signed( 7 downto 0);
  type sgn9_arr_t   is array(integer range <>) of   signed( 8 downto 0);
  type sgn10_arr_t  is array(integer range <>) of   signed( 9 downto 0);
  type sgn11_arr_t  is array(integer range <>) of   signed(10 downto 0);
  type sgn12_arr_t  is array(integer range <>) of   signed(11 downto 0);
  type sgn13_arr_t  is array(integer range <>) of   signed(12 downto 0);
  type sgn14_arr_t  is array(integer range <>) of   signed(13 downto 0);
  type sgn15_arr_t  is array(integer range <>) of   signed(14 downto 0);
  type sgn16_arr_t  is array(integer range <>) of   signed(15 downto 0);
  type sgn17_arr_t  is array(integer range <>) of   signed(16 downto 0);
  type sgn18_arr_t  is array(integer range <>) of   signed(17 downto 0);
  type sgn19_arr_t  is array(integer range <>) of   signed(18 downto 0);
  type sgn20_arr_t  is array(integer range <>) of   signed(19 downto 0);
  type sgn21_arr_t  is array(integer range <>) of   signed(20 downto 0);
  type sgn22_arr_t  is array(integer range <>) of   signed(21 downto 0);
  type sgn23_arr_t  is array(integer range <>) of   signed(22 downto 0);
  type sgn24_arr_t  is array(integer range <>) of   signed(23 downto 0);
  type sgn25_arr_t  is array(integer range <>) of   signed(24 downto 0);
  type sgn26_arr_t  is array(integer range <>) of   signed(25 downto 0);
  type sgn27_arr_t  is array(integer range <>) of   signed(26 downto 0);
  type sgn28_arr_t  is array(integer range <>) of   signed(27 downto 0);
  type sgn29_arr_t  is array(integer range <>) of   signed(28 downto 0);
  type sgn30_arr_t  is array(integer range <>) of   signed(29 downto 0);
  type sgn31_arr_t  is array(integer range <>) of   signed(30 downto 0);
  type sgn32_arr_t  is array(integer range <>) of   signed(31 downto 0);

  -- Functions
  function noop (ARG : signed) return signed;
  function noop (ARG : unsigned) return unsigned;
  function bo2sl  (ARG : boolean)   return std_logic;
  function bo2int (ARG : boolean)   return integer;
  function sl2int (ARG : std_logic) return integer;
  function any_one (ARG : std_logic_vector) return boolean;
  function unsigned_size_to_fit (ARG : natural) return natural;
  function resize_ok (ARG : signed; NEW_SIZE:natural) return boolean;
  function resize_ok (ARG : unsigned; NEW_SIZE:natural) return boolean;
  function myabs (ARG : signed) return signed;

end package;

package body misc_utils is

  -- Used to be able to index the result of an algebraic operation, like doing
  -- the shift right after a multiplication without defining an intermediate
  -- signal
  function noop (ARG : signed) return signed is
  begin
    return ARG;
  end function;

  function noop (ARG : unsigned) return unsigned is
  begin
    return ARG;
  end function;


  function bo2sl (ARG : boolean) return std_logic is
  begin
    if ARG then return '1';
    else        return '0';
    end if;
  end function;
  
  function bo2int (ARG : boolean) return integer is
  begin
    if ARG then return 1;
    else        return 0;
    end if;
  end function;
  
  function sl2int (ARG : std_logic) return integer is
  begin
    if ARG = '1' then return 1;
    else              return 0;
    end if;
  end function;

  -- almost an alias to make an or of a std_logic_vector and return as boolean
  function any_one (ARG : std_logic_vector) return boolean is
  begin
    return ARG /= (ARG'range => '0');
  end function;

  -- return the number of bits needed for an unsigned that is able to store ARG
  function unsigned_size_to_fit (ARG : natural) return natural is
  begin
    for i in 1 to 32 loop
      if 2**i - 1 >= ARG then return i;
      end if;
    end loop;
    return -1;
  end function;

  -- Returns if it is safe to resize this vector (i.e., the dropped bits
  -- don't contain any useful information)
  function resize_ok (ARG : signed; NEW_SIZE:natural) return boolean is
  begin
    for i in ARG'high - 1 downto ARG'high - (ARG'length - NEW_SIZE) loop
      if ARG(i) /= ARG(ARG'high) then return False;
      end if;
    end loop;
    return True;
  end function;

  function resize_ok (ARG : unsigned; NEW_SIZE:natural) return boolean is
  begin
    for i in ARG'high downto ARG'high + 1 - (ARG'length - NEW_SIZE) loop
      if ARG(i) /= '0' then return False;
      end if;
    end loop;
    return True;
  end function;

  -- Just replicates the behaviour of IEEE numeric_std's abs function
  -- I want that the abs value of the most negative value (whose true
  -- abs value does not fit a signed of its own width) is itself.
  -- The default IEEE function included with vivado behaves like that
  -- in simulation, but I have not been able to find proof that will
  -- also work like that in synthesis.
  function myabs (ARG : signed) return signed is
  begin
    return (ARG xor (ARG'range => ARG(ARG'high))) + ('0' & ARG(ARG'high));
  end function;
  
end package body;