library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.trigger_pkg.all;
use work.matcher_interface_pkg.all;

entity matcher_tb is
end entity;

architecture behavioral of matcher_tb is

  constant LHC_CLK_RATIO  : natural := 6;
  constant LHC_CLK_PERIOD : time := 25 ns;
  constant CLK_PERIOD     : time := LHC_CLK_PERIOD / LHC_CLK_RATIO;
  
  constant CSW : natural := 500;

  signal conf               : ControlRegs_t := CONTROLREGS_DEFAULT;

  signal clk                : std_logic := '1';
  signal rst                : std_logic := '1';
--  signal bunch_ctr          : unsigned(11 downto 0) := (others => '0');
  signal matcher_in         : matcher_in_t := MATCHER_IN_NULL;
  signal queue_out_arr      : matcher_out_t;

begin

  matcher_inst: entity work.matcher
  port map(
    clk               => clk                ,
    rst               => rst                ,
    conf              => conf               ,
    input             => matcher_in         ,
    queue_out_arr     => queue_out_arr
  );



  clk   <= not  clk after (  clk_period / 2.0 );
  
--  process(clk)
--    variable counter : natural range LHC_CLK_RATIO-1 downto 0 := 0;
--  begin
--    if rising_edge(clk) then
--      if counter > 0 then
--        counter := counter - 1;
--      else
--        counter := LHC_CLK_RATIO-1;
--        bunch_ctr <= bunch_ctr + 1;
--      end if;
--    end if;
--  end process;

  -- Several segments are injected in both SLs, at 3 waves. Each segment can be identified by 
  -- a different number in the hitID field of its ly0 hit. These are the pairings that should
  -- be evaluated, and the result of the fitting (some are invalidated by excessive chi2)
  -- 11 + 10 --> ok
  -- 5 + 9 --> no
  -- 11 + 9 --> no
  -- 5 + 1 --> no
  -- 5 + 4 --> no
  -- 5 + 3 --> ok
  -- 5 + 6 --> ok
  -- 7 + 1 --> no
  -- 7 + 3 --> ok
  -- 7 + 4 --> no
  -- 7 + 9 --> no
  -- 5 + 8 --> ok
  -- 7 + 6 --> ok
  -- 7 + 8 --> ok


  process
  begin
    rst <= '1';
    matcher_in <= MATCHER_IN_NULL;

    wait for (CLK_PERIOD * (LHC_CLK_RATIO * 2 + 1));

    rst <= '0';

    wait for (CLK_PERIOD * LHC_CLK_RATIO);

    -- WAVE 13
    matcher_in.sls(0).new_wave <= '1';
    matcher_in.sls(1).new_wave <= '1';
    matcher_in.sls(0).wave_bctr <= x"00D";
    matcher_in.sls(1).wave_bctr <= x"00D";
    wait for (clk_period);
    matcher_in.sls(0).new_wave <= '0';
    matcher_in.sls(1).new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 14
    matcher_in.sls(0).new_wave <= '1';
    matcher_in.sls(1).new_wave <= '1';
    matcher_in.sls(0).wave_bctr <= x"00E";
    matcher_in.sls(1).wave_bctr <= x"00E";
    wait for (clk_period);
    matcher_in.sls(0).new_wave <= '0';
    matcher_in.sls(1).new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 15
    matcher_in.sls(0).new_wave <= '1';
    matcher_in.sls(1).new_wave <= '1';
    matcher_in.sls(0).wave_bctr <= x"00F";
    matcher_in.sls(1).wave_bctr <= x"00F";
    wait for (clk_period);
    matcher_in.sls(0).new_wave <= '0';
    matcher_in.sls(1).new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    matcher_in.sls(1).segment_valid <= '1';
    matcher_in.sls(1).segment <= (
      t0              => x"00F" & "11111",
      position        => (others => '0'),
      is4hit          => '1'
    );
    wait for (clk_period);
    matcher_in.sls(1).segment_valid <= '1';
    matcher_in.sls(1).segment <= (
      t0              => x"00F" & "00000",
      position        => (others => '0'),
      is4hit          => '1'
    );
    wait for (clk_period);
    matcher_in.sls(1).segment_valid <= '0';
    matcher_in.sls(1).segment <= SLSEGMENT_NULL;
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 16
    matcher_in.sls(0).new_wave <= '1';
    matcher_in.sls(1).new_wave <= '1';
    matcher_in.sls(0).wave_bctr <= x"010";
    matcher_in.sls(1).wave_bctr <= x"010";
    wait for (clk_period);
    matcher_in.sls(0).new_wave <= '0';
    matcher_in.sls(1).new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    matcher_in.sls(1).segment_valid <= '1';
    matcher_in.sls(1).segment <= (
      t0              => x"010" & "11111",
      position        => (others => '0'),
      is4hit          => '1'
    );
    wait for (clk_period);
    matcher_in.sls(1).segment_valid <= '1';
    matcher_in.sls(1).segment <= (
      t0              => x"010" & "00000",
      position        => (others => '0'),
      is4hit          => '1'
    );
    wait for (clk_period);
    matcher_in.sls(1).segment_valid <= '0';
    matcher_in.sls(1).segment <= SLSEGMENT_NULL;
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 17
    matcher_in.sls(0).new_wave <= '1';
    matcher_in.sls(1).new_wave <= '1';
    matcher_in.sls(0).wave_bctr <= x"011";
    matcher_in.sls(1).wave_bctr <= x"011";
    wait for (clk_period);
    matcher_in.sls(0).new_wave <= '0';
    matcher_in.sls(1).new_wave <= '0';
    wait for (clk_period);
    matcher_in.sls(1).segment_valid <= '1';
    matcher_in.sls(1).segment <= (
      t0              => x"011" & "00000",
      position        => (others => '0'),
      is4hit          => '1'
    );
    wait for (clk_period);
    matcher_in.sls(0).segment_valid <= '1';
    matcher_in.sls(0).segment <= (
      t0              => x"011" & "00000",
      position        => (others => '0'),
      is4hit          => '1'
    );
    matcher_in.sls(1).segment_valid <= '1';
    matcher_in.sls(1).segment <= (
      t0              => x"011" & "00000",
      position        => (others => '0'),
      is4hit          => '0'
    );
    wait for (clk_period);
    matcher_in.sls(0).segment_valid <= '1';
    matcher_in.sls(0).segment <= (
      t0              => x"011" & "00000",
      position        => (others => '0'),
      is4hit          => '0'
    );
    matcher_in.sls(1).segment_valid <= '1';
    matcher_in.sls(1).segment <= (
      t0              => x"011" & "00000",
      position        => (9=>'1', others => '0'),
      is4hit          => '1'
    );
    wait for (clk_period);
    matcher_in.sls(0).segment_valid <= '1';
    matcher_in.sls(0).segment <= (
      t0              => x"011" & "00000",
      position        => (10=>'1', others => '0'),
      is4hit          => '1'
    );
    matcher_in.sls(1).segment_valid <= '1';
    matcher_in.sls(1).segment <= (
      t0              => x"011" & "00000",
      position        => (10=>'1', others => '0'),
      is4hit          => '1'
    );
    wait for (clk_period);
    matcher_in.sls(0).segment_valid <= '0';
    matcher_in.sls(0).segment <= SLSEGMENT_NULL;
    matcher_in.sls(1).segment_valid <= '0';
    matcher_in.sls(1).segment <= SLSEGMENT_NULL;
    wait for (clk_period * 6);

    -- WAVE 18
    matcher_in.sls(0).new_wave <= '1';
    matcher_in.sls(1).new_wave <= '1';
    matcher_in.sls(0).wave_bctr <= x"012";
    matcher_in.sls(1).wave_bctr <= x"012";
    wait for (clk_period);
    matcher_in.sls(0).new_wave <= '0';
    matcher_in.sls(1).new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 19
    matcher_in.sls(0).new_wave <= '1';
    matcher_in.sls(1).new_wave <= '1';
    matcher_in.sls(0).wave_bctr <= x"013";
    matcher_in.sls(1).wave_bctr <= x"013";
    wait for (clk_period);
    matcher_in.sls(0).new_wave <= '0';
    matcher_in.sls(1).new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 20
    matcher_in.sls(0).new_wave <= '1';
    matcher_in.sls(1).new_wave <= '1';
    matcher_in.sls(0).wave_bctr <= x"014";
    matcher_in.sls(1).wave_bctr <= x"014";
    wait for (clk_period);
    matcher_in.sls(0).new_wave <= '0';
    matcher_in.sls(1).new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 21
    matcher_in.sls(0).new_wave <= '1';
    matcher_in.sls(1).new_wave <= '1';
    matcher_in.sls(0).wave_bctr <= x"015";
    matcher_in.sls(1).wave_bctr <= x"015";
    wait for (clk_period);
    matcher_in.sls(0).new_wave <= '0';
    matcher_in.sls(1).new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);



    wait;
    
  end process;

end architecture;

